-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 27, 2019 at 01:16 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pupeta`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_dummies`
--

DROP TABLE IF EXISTS `data_dummies`;
CREATE TABLE `data_dummies` (
  `entitas` int(11) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `pagu` bigint(20) NOT NULL DEFAULT '0',
  `rencana_uang_rp` bigint(20) NOT NULL DEFAULT '0',
  `rencana_uang_persen` double NOT NULL DEFAULT '-1',
  `rencana_fisik_persen` double NOT NULL DEFAULT '0',
  `realisasi_uang_rp` bigint(20) NOT NULL DEFAULT '0',
  `realisasi_uang_persen` double NOT NULL DEFAULT '-1',
  `realisasi_fisik_persen` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_dummies`
--

INSERT INTO `data_dummies` (`entitas`, `provinsi`, `pagu`, `rencana_uang_rp`, `rencana_uang_persen`, `rencana_fisik_persen`, `realisasi_uang_rp`, `realisasi_uang_persen`, `realisasi_fisik_persen`) VALUES
(1, 'AC', 272413145000, 76885604167, 0, 48.14, 70445270879, 0, 31.75),
(2, 'BA', 139214250000, 15970973350, 0, 20.07, 15890253000, 0, 26),
(3, 'BT', 207819097000, 38361871974, 0, 14.27, 20667553224, 0, 6.51),
(4, 'BE', 81859238000, 24826483571, 0, 49.38, 15549557666, 0, 19.06),
(5, 'YO', 40629676000, 6533077990, 0, 11.7, 2445921600, 0, 8.5),
(6, 'GO', 98498834000, 12751004946, 0, 12.95, 1026679547, 0, 1.04),
(7, 'JK', 272982580000, 7078942881, 0, 0.81, 3215211435, 0, 0.05),
(8, 'JA', 58769507000, 20976040850, 0, 34.43, 15629766596, 0, 13.5),
(9, 'JB', 138420349000, 17311844035, 0, 9.89, 17070957035, 0, 1.58),
(10, 'JT', 141013508000, 18161177542, 0, 18.49, 11923970035, 0, 7.83),
(11, 'JI', 313278831000, 36848363711, 0, 5.55, 16593564840, 0, 1.21),
(12, 'KB', 336358614000, 53724545530, 0, 20.16, 37043381180, 0, 9.32),
(13, 'KS', 78153237000, 7587051400, 0, 26.36, 7027122000, 0, 11.89),
(14, 'KT', 54930421000, 7806032100, 0, 33.47, 5717876000, 0, 24.13),
(15, 'KI', 103989954000, 13031163000, 0, 14.09, 9665345000, 0, 10.37),
(16, 'KU', 146055777000, 78976776200, 0, 49.96, 67912450000, 0, 34.86),
(17, 'BB', 15421471000, 2000962800, 0, 32.05, 1648421350, 0, 7.34),
(18, 'ID', 118420121000, 75838274693, 0, 64.04, 41738130016, 0, 35.25),
(19, 'LA', 194474158000, 69959283743, 0, 25.74, 26397488343, 0, 11.02),
(20, 'MA', 312099646000, 105480033322, 0, 33.8, 39799775981, 0, 12.75),
(21, 'MU', 199612837000, 88075590577, 0, 47.62, 68499856000, 0, 34.23),
(22, 'NB', 119668940000, 22931804128, 0, 19.16, 16455080950, 0, 15.75),
(23, 'NT', 90599683000, 20396579620, 0, 20.64, 7487437820, 0, 8.26),
(24, 'PB', 644526473000, 312036206547, 0, 48.3, 252685437950, 0, 31.78),
(25, 'PA', 252232117000, 119997095783, 0, 47.57, 73008420424, 0, 28.94),
(26, 'RI', 448289049000, 82876850317, 0, 20.14, 55264669108, 0, 18.31),
(27, 'SR', 161865165000, 325283400, 0, 0.2, 25650000, 0, 0.02),
(28, 'SN', 338792267000, 53287967636, 0, 20.12, 32191864343, 0, 11.85),
(29, 'ST', 131744715000, 21135899307, 0, 0.51, 13663659760, 0, 47.42),
(30, 'SG', 141916728000, 35811541700, 0, 47.83, 39264250000, 0, 29.39),
(31, 'SA', 107796591000, 27815211788, 0, 25.8, 10124465260, 0, 9.39),
(32, 'SB', 242425855000, 5460308600, 0, 2.25, 5699434400, 0, 2.35),
(33, 'SS', 90863334000, 34341427790, 0, 34.35, 17642876725, 0, 27.44),
(34, 'SU', 164198095000, 43048553830, 0, 30.08, 28887518100, 0, 20.19);

-- --------------------------------------------------------

--
-- Table structure for table `data_progres_info`
--

DROP TABLE IF EXISTS `data_progres_info`;
CREATE TABLE `data_progres_info` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `tanggal` varchar(25) NOT NULL,
  `pekan` int(11) NOT NULL,
  `pagu` bigint(20) NOT NULL DEFAULT '0',
  `total_rencana_uang` double NOT NULL DEFAULT '0',
  `total_rencana_fisik` double NOT NULL DEFAULT '0',
  `total_realisasi_uang` double NOT NULL DEFAULT '0',
  `total_realisasi_fisik` double NOT NULL DEFAULT '0',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_progres_info`
--

INSERT INTO `data_progres_info` (`entitas`, `kode`, `provinsi`, `tanggal`, `pekan`, `pagu`, `total_rencana_uang`, `total_rencana_fisik`, `total_realisasi_uang`, `total_realisasi_fisik`, `terpakai`) VALUES
(1, 'cba2cff39394c5329f751d780b3c22df', 'BA', '13 oktober 2019', 3, 139214250000, 0, 35, 0, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_progres_isi`
--

DROP TABLE IF EXISTS `data_progres_isi`;
CREATE TABLE `data_progres_isi` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `progres` varchar(32) NOT NULL COMMENT '@data_progres_info(kode)',
  `paket` varchar(255) NOT NULL,
  `ksau` enum('K','S','AU') NOT NULL,
  `sycmyc` enum('SYC','MYC') NOT NULL,
  `pagu` bigint(20) NOT NULL DEFAULT '0',
  `nilai` bigint(20) NOT NULL DEFAULT '0',
  `rencana_uang_rp` bigint(20) NOT NULL DEFAULT '0',
  `rencana_uang_persen` double NOT NULL DEFAULT '-1',
  `rencana_fisik_persen` double NOT NULL DEFAULT '0',
  `realisasi_uang_rp` bigint(20) NOT NULL DEFAULT '0',
  `realisasi_uang_persen` double NOT NULL DEFAULT '-1',
  `realisasi_fisik_persen` double NOT NULL DEFAULT '0',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_progres_isi`
--

INSERT INTO `data_progres_isi` (`entitas`, `kode`, `progres`, `paket`, `ksau`, `sycmyc`, `pagu`, `nilai`, `rencana_uang_rp`, `rencana_uang_persen`, `rencana_fisik_persen`, `realisasi_uang_rp`, `realisasi_uang_persen`, `realisasi_fisik_persen`, `terpakai`) VALUES
(26, 'd400a2d19fbd64baa9e6b66f591c4bfd', 'cba2cff39394c5329f751d780b3c22df', 'ADMINISTRASI KEGIATAN', 'S', '', 40420000, 40420000, 4042000, -1, 25, 0, -1, 0, 1),
(27, 'eb7a4ec8dd90cef1ddad86258646a16e', 'cba2cff39394c5329f751d780b3c22df', 'HONOR OPERASIONAL SATUAN KERJA', 'S', '', 70110000, 70110000, 3505500, -1, 5, 0, -1, 0, 1),
(28, '2b124ea3ede32da9802f8de6a6a854ed', 'cba2cff39394c5329f751d780b3c22df', 'KONSULTAN PERENCANAAN REHABILITASI DAN RENOVASI SARANA DAN PRASARANA SEKOLAH', 'S', '', 55000000, 55000000, 0, -1, 0, 0, -1, 0, 1),
(29, '0f8121e0fca1dd057e2164827ff3d50c', 'cba2cff39394c5329f751d780b3c22df', 'MANAJEMEN KONSTRUKSI PEMBANGUNAN GEDUNG D4 POLITEKNIK NEGERI BALI', 'K', '', 1108000000, 829416000, 248824800, -1, 40, 165883000, -1, 33.5, 1),
(30, '328ab5e2573a4937fe1629d83637c57c', 'cba2cff39394c5329f751d780b3c22df', 'MANAJEMEN KONSTRUKSI PEMBANGUNAN GEDUNG PARKIR, LABORATORIUM MEDIA REKAM INSTITUT SENI INDONESIA DENPASAR', 'K', '', 1263000000, 1029105000, 308731500, -1, 40, 205821000, -1, 30, 1),
(31, '8cade7c56d9d22f622102923893eda2b', 'cba2cff39394c5329f751d780b3c22df', 'MANAJEMEN KONSTRUKSI PEMBANGUNAN PASAR SUKAWATI, KAB. GIANYAR', 'K', '', 800000000, 2180080000, 0, -1, 0, 0, -1, 0, 1),
(32, 'a866916f504bba7d234ff2ac91861c30', 'cba2cff39394c5329f751d780b3c22df', 'MONITORING LAPANGAN PPK', 'S', '', 48504000, 48504000, 9700800, -1, 25, 0, -1, 0, 1),
(33, '8856c0fc549838f2b8eb8949186324a3', 'cba2cff39394c5329f751d780b3c22df', 'PEMBANGUNAN GEDUNG D4 POLITEKNIK NEGERI BALI', 'K', '', 21710000000, 20090979000, 6027293700, -1, 30, 4018196000, -1, 22.558, 1),
(34, '00c2279b71fabb9daf7155e055e5947e', 'cba2cff39394c5329f751d780b3c22df', 'PEMBANGUNAN GEDUNG PARKIR, LABORATORIUM MEDIA REKAM INSTITUT SENI INDONESIA DENPASAR', 'K', '', 40565000000, 21862330000, 6558699000, -1, 30, 4372466000, -1, 13.6994, 1),
(35, '313d3180e315462a1c9658882c6d05de', 'cba2cff39394c5329f751d780b3c22df', 'PEMBANGUNAN PASAR SUKAWATI, KAB. GIANYAR', 'K', '', 31049000000, 74706500000, 0, -1, 0, 0, -1, 0, 1),
(36, '78fdb508a97c809e80649ddb66189982', 'cba2cff39394c5329f751d780b3c22df', 'PENDAMPINGAN BPKP', 'S', '', 321200000, 321200000, 0, -1, 0, 0, -1, 0, 1),
(37, 'bd4ecf16b146f9e0ec13f186f35a9e05', 'cba2cff39394c5329f751d780b3c22df', 'PENGAWASAN BERKALA PEMBANGUNAN GEDUNG D4 POLITEKNIK NEGERI BALI', 'K', '', 358000000, 358000000, 37656600, -1, 50, 0, -1, 0, 1),
(38, '7c65793e75c91d4348f0fa2d5beee4e5', 'cba2cff39394c5329f751d780b3c22df', 'PENGAWASAN BERKALA PEMBANGUNAN GEDUNG PARKIR, LABORATORIUM MEDIA REKAM INSTITUT SENI INDONESIA DENPASAR', 'K', '', 433000000, 433000000, 41406000, -1, 50, 0, -1, 0, 1),
(39, '99b61cb72f5da39d43ed3b2d56fbda9c', 'cba2cff39394c5329f751d780b3c22df', 'PENGAWASAN BERKALA PEMBANGUNAN PASAR SUKAWATI, KAB. GIANYAR', 'K', '', 186000000, 186000000, 0, -1, 0, 0, -1, 0, 1),
(40, 'aca715829efa605d90fb25c3fa35344e', 'cba2cff39394c5329f751d780b3c22df', 'PENGELOLAAN KEGIATAN PEMBANGUNAN PASAR SUKAWATI, KAB. GIANYAR', 'K', '', 197000000, 197000000, 39400000, -1, 25, 0, -1, 0, 1),
(41, 'f1bca1dc8e44f74d57365382b29a59a2', 'cba2cff39394c5329f751d780b3c22df', 'PENGELOLAAN KEGIATAN REHAB DAN RENOV SARPRAS MADRASAH KAB. KARANGASEM, KAB. JEMBRANA, KAB. BULELENG', 'S', '', 280000000, 280000000, 112000000, -1, 50, 75390000, -1, 30, 1),
(42, 'f771c7ad7ab4abe06c9e8f02a027b971', 'cba2cff39394c5329f751d780b3c22df', 'PENGELOLAAN KEGIATAN REHABILITASI DAN RENOVASI SARANA PRASARANA SEKOLAH', 'S', '', 281000000, 281000000, 112400000, -1, 50, 63867000, -1, 30, 1),
(43, 'b3890314e2c71b28989b121a5b04c93b', 'cba2cff39394c5329f751d780b3c22df', 'PENGELOLAAN PEMBANGUNAN GEDUNG D4 POLITEKNIK NEGERI BALI', 'S', '', 306000000, 306000000, 122400000, -1, 50, 72284000, -1, 30, 1),
(44, 'b0bd9a79496d08b1fdd71c6630511244', 'cba2cff39394c5329f751d780b3c22df', 'PENGELOLAAN PEMBANGUNAN GEDUNG PARKIR, LABORATORIUM MEDIA REKAM INSTITUT SENI INDONESIA DENPASAR', 'S', '', 342000000, 342000000, 136800000, -1, 50, 80915000, -1, 30, 1),
(45, 'df66477835d66494f722416bdc610a6e', 'cba2cff39394c5329f751d780b3c22df', 'RAPAT KOORDINASI AWAL DAN AKHIR', 'S', '', 244772000, 244772000, 0, -1, 0, 0, -1, 0, 1),
(46, '415f2e6b9e7547d9a13cc875b7d5c514', 'cba2cff39394c5329f751d780b3c22df', 'REHABILITASI DAN RENOVASI SARANA PRASARANA MADRASAH KAB. KARANGASEM, KAB. JEMBRANA, KAB. BULELENG', 'K', '', 18830800000, 16045758000, 4813727400, -1, 30, 3209152000, -1, 13.594, 1),
(47, '95d5a32eeaef2262489a59bc80adeca5', 'cba2cff39394c5329f751d780b3c22df', 'REHABILITASI DAN RENOVASI SARANA PRASARANA SEKOLAH KAB. KARANGASEM, KAB. KLUNGKUNG', 'K', '', 19016094000, 16928866000, 5078659800, -1, 30, 3385773000, -1, 24.63, 1),
(48, '8ba2fa3f8a70fb2afa4c56f9e45aeb2b', 'cba2cff39394c5329f751d780b3c22df', 'SUPERVISI REHABILITASI DAN RENOVASI SARANA PRASARANA MADRASAH KAB. KARANGASEM, KAB. JEMBRANA, KAB. BULELENG', 'K', '', 795000000, 548625000, 164587500, -1, 30, 109725000, -1, 25, 1),
(49, '38f07798171714da0146c5a9659706a5', 'cba2cff39394c5329f751d780b3c22df', 'SUPERVISI REHABILITASI DAN RENOVASI SARANA PRASARANA SEKOLAH KAB. KARANGASEM, KAB. KLUNGKUNG', 'K', '', 801000000, 538153000, 161445900, -1, 30, 107631000, -1, 25, 1),
(50, 'b9f0b7d06a4931bdca06e3e7df3b8475', 'cba2cff39394c5329f751d780b3c22df', 'UANG MAKAN/UANG LEMBUR', 'S', '', 113350000, 113350000, 34005000, -1, 35, 23150000, -1, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_progres_rinci`
--

DROP TABLE IF EXISTS `data_progres_rinci`;
CREATE TABLE `data_progres_rinci` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `isi` varchar(32) NOT NULL COMMENT '@data_progres_isi(kode)',
  `sarana` varchar(255) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `jenjang` varchar(50) NOT NULL,
  `pagu` bigint(20) NOT NULL DEFAULT '0',
  `nilai` bigint(20) NOT NULL DEFAULT '0',
  `revisi` bigint(20) NOT NULL DEFAULT '0',
  `rencana_uang_rp` bigint(20) NOT NULL DEFAULT '0',
  `rencana_uang_persen` double NOT NULL DEFAULT '-1',
  `rencana_fisik_persen` double NOT NULL DEFAULT '0',
  `realisasi_uang_rp` bigint(20) NOT NULL DEFAULT '0',
  `realisasi_uang_persen` double NOT NULL DEFAULT '-1',
  `realisasi_fisik_persen` double NOT NULL DEFAULT '0',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_progres_rinci`
--

INSERT INTO `data_progres_rinci` (`entitas`, `kode`, `isi`, `sarana`, `lokasi`, `jenjang`, `pagu`, `nilai`, `revisi`, `rencana_uang_rp`, `rencana_uang_persen`, `rencana_fisik_persen`, `realisasi_uang_rp`, `realisasi_uang_persen`, `realisasi_fisik_persen`, `terpakai`) VALUES
(1, 'b0bbf5c9ce76dba5071144d7debd1717', '502d5d0987d52b1b43bc6aecc0acfd92', 'SD 2 JUNGUTAN', 'KAB KARANGASEM', 'SD', 2123279000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(2, '078c6aa63c39f2b2c7989e332687eb4c', '502d5d0987d52b1b43bc6aecc0acfd92', 'SD 4 JUNGUTAN', 'KAB KARANGASEM', 'SD', 2076527000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(3, 'ff82ca3a466e0dceaae3d1fe0b0e0918', '502d5d0987d52b1b43bc6aecc0acfd92', 'SD 6 JUNGUTAN', 'KAB KARANGASEM', 'SD', 2757273000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(4, '9eedc9070d71e80e3e668e5d09adcfe7', '502d5d0987d52b1b43bc6aecc0acfd92', 'SMP SATU ATAP JUNGUTAN', 'KAB KARANGASEM', 'SMP', 2646343000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(5, '6c90165084de486c8ac1440b2d72ec52', '502d5d0987d52b1b43bc6aecc0acfd92', 'SD 4 GEGELANG', 'KAB KARANGASEM', 'SD', 3737910000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(6, 'dc4ff1b6b2698be73d2e31a46fc8bc5d', '502d5d0987d52b1b43bc6aecc0acfd92', 'SD 7 GEGELANG', 'KAB KARANGASEM', 'SD', 3614939000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(7, '038b49f7c2977f961aec36638cdc01db', '502d5d0987d52b1b43bc6aecc0acfd92', 'SD 4 KUSAMBA', 'KAB KLUNGKUNG', 'SD', 2059823000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(8, 'd08f057df4b32e881a7564bb3188a25b', '95d5a32eeaef2262489a59bc80adeca5', 'SD 2 JUNGUTAN', 'KAB KARANGASEM', 'SD', 2123279000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(9, 'a1eed490c05daab299e76869b19cb540', '95d5a32eeaef2262489a59bc80adeca5', 'SD 4 JUNGUTAN', 'KAB KARANGASEM', 'SD', 2076527000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(10, 'ac7c61eb29659d1be85676842ee3417a', '95d5a32eeaef2262489a59bc80adeca5', 'SD 6 JUNGUTAN', 'KAB KARANGASEM', 'SD', 2757273000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(11, '1b26818529ba476c1c32127a6d4082fb', '95d5a32eeaef2262489a59bc80adeca5', 'SMP SATU ATAP JUNGUTAN', 'KAB KARANGASEM', 'SMP', 2646343000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(12, '6125864bd950924dcc27dc46feddb6e0', '95d5a32eeaef2262489a59bc80adeca5', 'SD 4 GEGELANG', 'KAB KARANGASEM', 'SD', 3737910000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(13, 'c8b905e561fd97aad366651f65df9901', '95d5a32eeaef2262489a59bc80adeca5', 'SD 7 GEGELANG', 'KAB KARANGASEM', 'SD', 3614939000, 0, 0, 0, -1, 0, 0, -1, 0, 1),
(14, '2a768626723d6d87419d741305f01bdd', '95d5a32eeaef2262489a59bc80adeca5', 'SD 4 KUSAMBA', 'KAB KLUNGKUNG', 'SD', 2059823000, 0, 0, 0, -1, 0, 0, -1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_provinsi`
--

DROP TABLE IF EXISTS `data_provinsi`;
CREATE TABLE `data_provinsi` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `kode_provinsi` varchar(2) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_provinsi`
--

INSERT INTO `data_provinsi` (`entitas`, `kode`, `kode_provinsi`, `nama`, `terpakai`) VALUES
(11, '636a970a6c83c1aa368573bd244a4594', 'AC', 'ACEH', 1),
(12, 'b374d02903fc1a4aebcdb2f4e40086ad', 'SU', 'SUMATERA UTARA', 1),
(13, '600b24e428a7b053bd196895fc548924', 'SB', 'SUMATERA BARAT', 1),
(14, 'f1087c13bf361c1ac6da7eecedb3e66d', 'RI', 'RIAU', 1),
(15, '5ead3935e4e232b5380604fb3a723be4', 'JA', 'JAMBI', 1),
(16, 'ca7ae5654609e97df486b8836766227c', 'SS', 'SUMATERA SELATAN', 1),
(17, 'b944f609828f1a0b2564bba342a7e965', 'BE', 'BENGKULU', 1),
(18, 'c0fd81cf0e590f35c696eaa0d84de6df', 'LA', 'LAMPUNG', 1),
(19, '217e658752eff46d346f5f798ad27363', 'BB', 'KEPULAUAN BANGKA BELITUNG', 1),
(21, 'a5261a78bc7971eb50f408d5888af166', 'ID', 'KEPULAUAN RIAU', 1),
(31, 'eeb5dfb5ea0f662725023a3901491f68', 'JK', 'DKI JAKARTA', 1),
(32, '48a61ae2cffbe8557387c7ff4748ffa9', 'JB', 'JAWA BARAT', 1),
(33, '7ed008b01075f5665ff4f0b2dcc37403', 'JT', 'JAWA TENGAH', 1),
(34, '629d3fd292355af4f772d2a8b346507f', 'YO', 'DI YOGYAKARTA', 1),
(35, '68b0cee7feb7eabab997f8d8ef2a8493', 'JI', 'JAWA TIMUR', 1),
(36, '1d2033e8af05d336a6f20619e015be83', 'BT', 'BANTEN', 1),
(51, '87c7ffbd0bfddfbcb88fb7f606736393', 'BA', 'BALI', 1),
(52, 'eddd0adfdb6913b27fd1111710428cdc', 'NB', 'NUSA TENGGARA BARAT', 1),
(53, 'c8b9b42ec5e42e8c9bbebc76e7542653', 'NT', 'NUSA TENGGARA TIMUR', 1),
(61, 'f640fc9a9e241ad54026a8a65bb55561', 'KB', 'KALIMANTAN BARAT', 1),
(62, 'bed1043150f54490ac1809419755e811', 'KT', 'KALIMANTAN TENGAH', 1),
(63, '6d8f796030a2627defb036a5d333a4cd', 'KS', 'KALIMANTAN SELATAN', 1),
(64, '7f0d504d35b5a98f1ca84c61916a843c', 'KI', 'KALIMANTAN TIMUR', 1),
(65, '92e653e448ae112d5860b40722fa1fd5', 'KU', 'KALIMANTAN UTARA', 1),
(71, 'e867ebdf2ad659873dd2d49fa154aee7', 'SA', 'SULAWESI UTARA', 1),
(72, 'fb7d1eeb0d2d27286bcdc0b91ed69127', 'ST', 'SULAWESI TENGAH', 1),
(73, '2bde3cd7581483a3523348de05b78d81', 'SN', 'SULAWESI SELATAN', 1),
(74, '634c1c4ace29e5e9d0916ccfa71ceca2', 'SG', 'SULAWESI TENGGARA', 1),
(75, '28fecb1f19a403f89efcbf08e063ecf9', 'GO', 'GORONTALO', 1),
(76, '52a1293e5d1f8af08b828ac4453c0cc4', 'SR', 'SULAWESI BARAT', 1),
(81, '7a8264e070620e5b80e4a9f68bcd7776', 'MA', 'MALUKU', 1),
(82, 'c0f7d9b9a82df8ac2bd0824b2c63e2f2', 'MU', 'MALUKU UTARA', 1),
(91, '9bf59dff527d7097e9e239bc643b8a8d', 'PA', 'PAPUA BARAT', 1),
(94, '836cc43389531cde000d5bba01bc2a30', 'PB', 'PAPUA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_sesi`
--

DROP TABLE IF EXISTS `data_sesi`;
CREATE TABLE `data_sesi` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_dummies`
--
ALTER TABLE `data_dummies`
  ADD PRIMARY KEY (`entitas`);

--
-- Indexes for table `data_progres_info`
--
ALTER TABLE `data_progres_info`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_progres_isi`
--
ALTER TABLE `data_progres_isi`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_progres_rinci`
--
ALTER TABLE `data_progres_rinci`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_provinsi`
--
ALTER TABLE `data_provinsi`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_sesi`
--
ALTER TABLE `data_sesi`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_dummies`
--
ALTER TABLE `data_dummies`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `data_progres_info`
--
ALTER TABLE `data_progres_info`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_progres_isi`
--
ALTER TABLE `data_progres_isi`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `data_progres_rinci`
--
ALTER TABLE `data_progres_rinci`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `data_provinsi`
--
ALTER TABLE `data_provinsi`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
