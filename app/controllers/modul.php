<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modul
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Modul extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        self::tampil();
    }

    public function tampil($halaman = 'info_peta', $folder = NULL) {
        if ($folder != NULL) {
            $this->load->view($folder . '/' . $halaman);
        } else {
            $this->load->view('peta/' . $halaman);
        }
    }

    public function proses($tipe) {
        $path = 'etc/data/' . $tipe . '.xls';

        if (file_exists($path)) {
            $this->load->library('excelutil');
            // Read the file
            $inputFileType = PHPExcel_IOFactory::identify('./' . $path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load('./' . $path);
            $objPHPExcel->setActiveSheetIndex(0); // activate worksheet number 1
            $sheet = $objPHPExcel->getSheet(0); // Get worksheet dimensions

            if ($tipe === 'progres') {
                self::_doProgres($sheet);
            } else if ($tipe === 'rincian') {
                self::_doRinci($sheet);
            }
        }
    }

    public function petaan() {
        $data = array();

        foreach ($this->model->getList(array('table' => 'data_provinsi', 'where' => array('terpakai' => 1))) as $dataProvinsi) {
            $dataProgres = $this->model->getRecord(array('table' => 'data_progres_info', 'where' => array('provinsi' => strtoupper($dataProvinsi->kode_provinsi))));
            $dataBodo = $this->model->getRecord(array('table' => 'data_dummies', 'where' => array('provinsi' => strtoupper($dataProvinsi->kode_provinsi))));
            $data[$dataProvinsi->kode_provinsi]['nama'] = strtoupper($dataProvinsi->nama);
            $pagu = 0;
            $rencanaUangInt = 0;
            $rencanaUangFloat = 0.0;
            $rencanaFisik = 0.0;
            $realisasiUangInt = 0;
            $realisasiUangFloat = 0.0;
            $realisasiFisik = 0.0;

            if ($dataProgres != NULL) {
                foreach ($this->model->getList(array('table' => 'data_progres_isi', 'where' => array('progres' => $dataProgres->kode))) as $dataIsi) {
                    $pagu += $dataIsi->pagu;
                    $rencanaUangInt += $dataIsi->rencana_uang_rp;
                    $realisasiUangInt += $dataIsi->realisasi_uang_rp;
                }

                $rencanaFisik = $dataProgres->total_rencana_fisik;
                $realisasiFisik = $dataProgres->total_realisasi_fisik;
            } else {
                if ($dataBodo != NULL) {
                    $pagu = $dataBodo->pagu;
                    $rencanaUangInt = $dataBodo->rencana_uang_rp;
                    $rencanaUangFloat = $dataBodo->rencana_uang_persen;
                    $rencanaFisik = $dataBodo->rencana_fisik_persen;
                    $realisasiUangInt = $dataBodo->realisasi_uang_rp;
                    $realisasiUangFloat = $dataBodo->realisasi_uang_persen;
                    $realisasiFisik = $dataBodo->realisasi_fisik_persen;
                }
            }

            if ($pagu > 0) {
                $rencanaUangFloat = round(($rencanaUangInt / $pagu) * 100);
                $realisasiUangFloat = round(($realisasiUangInt / $pagu) * 100);
            }

            $data[$dataProvinsi->kode_provinsi]['pagu'] = $pagu;
            $data[$dataProvinsi->kode_provinsi]['rencana_keuangan_nominal'] = $rencanaUangInt;
            $data[$dataProvinsi->kode_provinsi]['rencana_keuangan_persen'] = $rencanaUangFloat;
            $data[$dataProvinsi->kode_provinsi]['rencana_fisik'] = $rencanaFisik;
            $data[$dataProvinsi->kode_provinsi]['realisasi_keuangan_nominal'] = $realisasiUangInt;
            $data[$dataProvinsi->kode_provinsi]['realisasi_keuangan_persen'] = $realisasiUangFloat;
            $data[$dataProvinsi->kode_provinsi]['realisasi_fisik'] = $realisasiFisik;
        }

        echo json_encode(array('data' => $data));
    }

    public function doBodo() {
        $path = 'etc/dummies.xls';

        if (file_exists($path)) {
            $this->load->library('excelutil');
            // Read the file
            $inputFileType = PHPExcel_IOFactory::identify('./' . $path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load('./' . $path);
            $objPHPExcel->setActiveSheetIndex(0); // activate worksheet number 1
            $sheet = $objPHPExcel->getSheet(0); // Get worksheet dimensions

            for ($row = 1; $row <= $sheet->getHighestRow(); $row++) { // show - Loop through each row of the worksheet in turn
                $dataProvinsi = $this->model->getRecord(array('table' => 'data_provinsi', 'where' => array('nama' => strtoupper(trim($sheet->getCell('A' . $row)->getValue())))));

                if ($dataProvinsi != NULL) {
                    $dataBodoh = $this->model->getRecord(array('table' => 'data_dummies', 'where' => array('provinsi' => strtoupper($dataProvinsi->kode_provinsi))));
                    $isiBodoh = array(
                        'provinsi' => strtoupper($dataProvinsi->kode_provinsi),
                        'pagu' => trim($sheet->getCell('B' . $row)->getValue()),
                        'rencana_uang_rp' => trim($sheet->getCell('C' . $row)->getValue()),
                        'rencana_uang_persen' => trim($sheet->getCell('D' . $row)->getValue()),
                        'rencana_fisik_persen' => trim($sheet->getCell('E' . $row)->getValue()),
                        'realisasi_uang_rp' => trim($sheet->getCell('F' . $row)->getValue()),
                        'realisasi_uang_persen' => trim($sheet->getCell('G' . $row)->getValue()),
                        'realisasi_fisik_persen' => trim($sheet->getCell('H' . $row)->getValue())
                    );
                    $idBodoh = 0;
                    $aksiBodoh = $this->model->CREATE;

                    if ($dataBodoh != NULL) {
                        $aksiBodoh = $this->model->UPDATE;
                        $idBodoh = $dataBodoh->entitas;
                    }

                    $this->model->action(array(
                        'table' => 'data_dummies', 'type' => $aksiBodoh,
                        'data' => $isiBodoh, 'at' => array('entitas' => $idBodoh)
                    ));
                }
            }
        }

        redirect(site_url());
    }

    private function _doProgres($sheet) {
        $provinsi = trim($sheet->getCell('E3')->getValue());
        $tanggal = trim($sheet->getCell('E4')->getValue());
        $pekan = trim($sheet->getCell('E5')->getValue());
        $paguRev = trim($sheet->getCell('E6')->getValue());

        if ($provinsi !== '' && $pekan !== '') {
            $dataProvinsi = $this->model->getRecord(array('table' => 'data_provinsi', 'where' => array('nama' => strtoupper($provinsi))));

            if ($dataProvinsi != NULL) {
                $dataProgres = $this->model->getRecord(array('table' => 'data_progres_info', 'where' => array('provinsi' => strtoupper($dataProvinsi->kode_provinsi), 'pekan' => intval($pekan))));
                $aksiProgres = $this->model->CREATE;
                $idProgres = 0;
                $isiProgres = array(
                    'kode' => random_string('unique'), 'terpakai' => 1,
                    'provinsi' => strtoupper($dataProvinsi->kode_provinsi), 'tanggal' => strtolower($tanggal),
                    'pekan' => intval($pekan), 'pagu' => $paguRev,
                    'total_rencana_uang' => -1, 'total_rencana_fisik' => -1, 'total_realisasi_uang' => -1,
                    'total_realisasi_fisik' => -1
                );

                if ($dataProgres != NULL) {
                    $aksiProgres = $this->model->UPDATE;
                    $idProgres = $dataProgres->entitas;
                    $isiProgres['kode'] = $dataProgres->kode;
                }

                $aman = $this->model->action(array(
                    'table' => 'data_progres_info', 'type' => $aksiProgres,
                    'data' => $isiProgres, 'at' => array('entitas' => $idProgres)
                ));

                if ($aman) {
                    $hapus = $this->model->action(array(
                        'table' => 'data_progres_isi', 'type' => $this->model->DELETE,
                        'data' => array(), 'at' => array('progres' => $isiProgres['kode'])
                    ));

                    if ($hapus) {
                        // show - Loop through each row of the worksheet in turn
                        for ($row = 10; $row <= $sheet->getHighestRow(); $row++) {
                            if (strtolower(trim($sheet->getCell('A' . $row)->getValue())) !== 'total') {
                                $paguPakai = trim($sheet->getCell('E' . $row)->getValue());
                                $kontrak = trim($sheet->getCell('F' . $row)->getValue());
                                $rencanaUangRP = trim($sheet->getCell('G' . $row)->getValue());
                                $rencanaUangPersen = trim($sheet->getCell('H' . $row)->getValue());
                                $rencanaFisikPersen = trim($sheet->getCell('I' . $row)->getValue());
                                $realisasiUangRP = trim($sheet->getCell('J' . $row)->getValue());
                                $realisasiUangPersen = trim($sheet->getCell('K' . $row)->getValue());
                                $realisasiFisikPersen = trim($sheet->getCell('L' . $row)->getValue());

                                if ($paguPakai === '') {
                                    $paguPakai = '0';
                                }

                                if ($kontrak === '') {
                                    $kontrak = '0';
                                }

                                if ($rencanaUangRP === '') {
                                    $rencanaUangRP = '0';
                                }

                                if ($rencanaUangPersen === '') {
                                    $rencanaUangPersen = -1;
                                }

                                if ($rencanaFisikPersen === '') {
                                    $rencanaFisikPersen = '0';
                                }

                                if ($realisasiUangRP === '') {
                                    $realisasiUangRP = '0';
                                }

                                if ($realisasiUangPersen === '') {
                                    $realisasiUangPersen = -1;
                                }

                                if ($realisasiFisikPersen === '') {
                                    $realisasiFisikPersen = '0';
                                }

                                $this->model->action(array(
                                    'table' => 'data_progres_isi', 'type' => $this->model->CREATE,
                                    'data' => array(
                                        'kode' => random_string('unique'), 'terpakai' => 1,
                                        'progres' => $isiProgres['kode'],
                                        'paket' => strtoupper(trim($sheet->getCell('B' . $row)->getValue())),
                                        'ksau' => strtoupper(trim($sheet->getCell('C' . $row)->getValue())),
                                        'sycmyc' => strtoupper(trim($sheet->getCell('D' . $row)->getValue())),
                                        'pagu' => $paguPakai, 'nilai' => $kontrak,
                                        'rencana_uang_rp' => $rencanaUangRP, 'rencana_uang_persen' => $rencanaUangPersen,
                                        'rencana_fisik_persen' => $rencanaFisikPersen,
                                        'realisasi_uang_rp' => $realisasiUangRP, 'realisasi_uang_persen' => $realisasiUangPersen,
                                        'realisasi_fisik_persen' => $realisasiFisikPersen
                                    ), 'at' => array('entitas' => 0)
                                ));
                            }
                        }

                        $row--;
                        $isiProgres['total_rencana_uang'] = trim($sheet->getCell('H' . $row)->getValue());
                        $isiProgres['total_rencana_fisik'] = trim($sheet->getCell('I' . $row)->getValue());
                        $isiProgres['total_realisasi_uang'] = trim($sheet->getCell('K' . $row)->getValue());
                        $isiProgres['total_realisasi_fisik'] = trim($sheet->getCell('L' . $row)->getValue());
                        $this->model->action(array(
                            'table' => 'data_progres_info', 'type' => $this->model->UPDATE,
                            'data' => $isiProgres, 'at' => array('kode' => $isiProgres['kode'])
                        ));
                    }
                }
            }
        }

        redirect(site_url('modul/tampil/peta/data'));
    }

    private function _doRinci($sheet) {
        $provinsi = trim($sheet->getCell('D3')->getValue());
        $paket = trim($sheet->getCell('D4')->getValue());
        $tanggal = trim($sheet->getCell('D7')->getValue());

        if ($provinsi !== '' && $paket !== '' && $tanggal !== '') {
            $dataProvinsi = $this->model->getRecord(array('table' => 'data_provinsi', 'where' => array('nama' => strtoupper($provinsi))));

            if ($dataProvinsi != NULL) {
                $dataProgres = $this->model->getRecord(array('table' => 'data_progres_info', 'where' => array('provinsi' => strtoupper($dataProvinsi->kode_provinsi), 'tanggal' => strtolower($tanggal))));

                if ($dataProgres != NULL) {
                    $dataIsi = $this->model->getRecord(array('table' => 'data_progres_isi', 'where' => array('progres' => $dataProgres->kode, 'paket' => strtoupper($paket))));

                    if ($dataIsi != NULL) {
                        $hapus = $this->model->action(array(
                            'table' => 'data_progres_rinci', 'type' => $this->model->DELETE,
                            'data' => array(), 'at' => array('isi' => $dataIsi->kode)
                        ));

                        if ($hapus) {
                            // show - Loop through each row of the worksheet in turn
                            for ($row = 11; $row <= $sheet->getHighestRow(); $row++) {
                                if (strtolower(trim($sheet->getCell('A' . $row)->getValue())) !== 'total') {
                                    $paguPakai = trim($sheet->getCell('E' . $row)->getValue());
                                    $kontrak = trim($sheet->getCell('F' . $row)->getValue());
                                    $revisi = trim($sheet->getCell('G' . $row)->getValue());
                                    $rencanaUangRP = trim($sheet->getCell('H' . $row)->getValue());
                                    $rencanaUangPersen = trim($sheet->getCell('I' . $row)->getValue());
                                    $rencanaFisikPersen = trim($sheet->getCell('J' . $row)->getValue());
                                    $realisasiUangRP = trim($sheet->getCell('K' . $row)->getValue());
                                    $realisasiUangPersen = trim($sheet->getCell('L' . $row)->getValue());
                                    $realisasiFisikPersen = trim($sheet->getCell('M' . $row)->getValue());

                                    if ($paguPakai === '') {
                                        $paguPakai = '0';
                                    }

                                    if ($kontrak === '') {
                                        $kontrak = '0';
                                    }

                                    if ($revisi === '') {
                                        $revisi = '0';
                                    }

                                    if ($rencanaUangRP === '') {
                                        $rencanaUangRP = '0';
                                    }

                                    if ($rencanaUangPersen === '') {
                                        $rencanaUangPersen = -1;
                                    }

                                    if ($rencanaFisikPersen === '') {
                                        $rencanaFisikPersen = '0';
                                    }

                                    if ($realisasiUangRP === '') {
                                        $realisasiUangRP = '0';
                                    }

                                    if ($realisasiUangPersen === '') {
                                        $realisasiUangPersen = -1;
                                    }

                                    if ($realisasiFisikPersen === '') {
                                        $realisasiFisikPersen = '0';
                                    }

                                    $this->model->action(array(
                                        'table' => 'data_progres_rinci', 'type' => $this->model->CREATE,
                                        'data' => array(
                                            'kode' => random_string('unique'), 'terpakai' => 1,
                                            'isi' => $dataIsi->kode,
                                            'sarana' => strtoupper(trim($sheet->getCell('B' . $row)->getValue())),
                                            'lokasi' => strtoupper(trim($sheet->getCell('C' . $row)->getValue())),
                                            'jenjang' => strtoupper(trim($sheet->getCell('D' . $row)->getValue())),
                                            'pagu' => $paguPakai, 'nilai' => $kontrak, 'revisi' => $revisi,
                                            'rencana_uang_rp' => $rencanaUangRP, 'rencana_uang_persen' => $rencanaUangPersen,
                                            'rencana_fisik_persen' => $rencanaFisikPersen,
                                            'realisasi_uang_rp' => $realisasiUangRP, 'realisasi_uang_persen' => $realisasiUangPersen,
                                            'realisasi_fisik_persen' => $realisasiFisikPersen
                                        ), 'at' => array('entitas' => 0)
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }

        redirect(site_url('modul/tampil/peta/data'));
    }

}
