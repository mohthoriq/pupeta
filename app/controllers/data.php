<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of data
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Data extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect(site_url());
    }

    // create, update, or delete
    public function simpan() {
        /*
         * code info:
         * 	- 0 = akses tidak sah (user undefined)
         * 	- 1 = proses berhasil
         * 	- 2 = proses gagal
         * 	- 3 = input invalid
         */
        $code = 0;
        $message = '';
        /* collect request */
        $model = 'model' . $this->input->post('model-input', TRUE); // use XSS Filtering
        $this->load->model($model);
        $this->form_validation->set_rules($this->$model->getRules($this->input->post('action-input', TRUE))); // create, update, delete rules (use XSS Filtering)

        if ($this->form_validation->run() == FALSE) { // Inject to SweetAlert
            $delimiter = '<i class="fa fa-exclamation"></i> ';
            $this->form_validation->set_error_delimiters($delimiter, '<br>');
            $message = validation_errors();
            $code = 3;
        } else {
            $result = $this->$model->doAction($this->input->post(NULL)); // not use XSS Filtering
            $code = ($result) ? 1 : 2;
        }

        echo json_encode(array('return' => array(
                'code' => $code,
                'message' => $message
        )));
    }

    public function kodean() {
        echo json_encode(array('data' => random_string('unique')));
    }

    // view record & records
    public function detail() {
        $model = 'model' . $this->input->post('param', TRUE); // use XSS Filtering
        $this->load->model($model);
        echo json_encode(array('data' => $this->$model->getData($this->input->post('kode', TRUE)))); // use XSS Filtering
    }

    public function tabel($name, $query = NULL) {
        $model = 'model' . $name;
        $this->load->model($model);
        echo json_encode(array('data' => $this->$model->getTabel($query))); // DATATABLES
    }

    public function pilih($name, $query = NULL) {
        $model = 'model' . $name;
        $this->load->model($model);

        if ($this->input->post('term', TRUE) !== FALSE) { // use XSS Filtering
            if ($query == NULL) {
                $query = $this->input->post('term', TRUE); // use XSS Filtering
            } else {
                $query .= '___' . $this->input->post('term', TRUE); // use XSS Filtering
            }
        }

        echo json_encode(array('results' => $this->$model->getPilih($query))); // SELECT2JS
    }

    public function unggah() {
        if (isset($_FILES['filedocument'])) {
            // upload setting
            $request = array(// get the files posted
                'input' => 'filedocument[]',
                'file' => $_FILES['filedocument']
            );
            $inputs = $this->input->post(null, TRUE); // use XSS Filtering
            $path = $inputs['path'] . '/' . $inputs['dir'];
            echo self::_upload($request, $path); // start
        }
    }

    public function hapusGambar() {
        $code = 0;
        $image = $this->input->post('path', TRUE); // use XSS Filtering

        if ($image !== FALSE) {
            $code = (unlink('./etc/' . $image)) ? 1 : 0;
        }

        echo json_encode(array('code' => $code));
    }

    private function _upload($request, $path) {
        $error = array('error' => 'Proses unggah berkas gagal');

        if (self::_dirInit($path)) {
            $this->load->library('upload', array(
                'allowed_types' => '*',
                'overwrite' => true,
                'upload_path' => './' . $path . '/', // file path ...
                'max_size' => 20000 // maximum size 20 MB (in kilobytes)
            )); // upload config
            $fails = self::_doSave($request);

            if ($fails == 0) {
                $error = array();
            } else {
                $error = array('error' => $fails . ' proses unggah berkas gagal');
            }
        }

        return json_encode($error); // return a json encoded response to let user know upload results
    }

    private function _dirInit($path) {
        $continue = FALSE;

        if (!file_exists($path)) {
            if (!is_dir($path)) {
                $continue = (!file_exists($path)) ? mkdir($path, 0777, TRUE) : TRUE;
            } else {
                $continue = TRUE;
            }
        } else {
            $continue = TRUE;
        }

        return $continue;
    }

    private function _doSave($request) {
        $fails = 0;

        /* upload begin */
        foreach ($request['file']['name'] as $at => $file) {
            // mapping files
            $_FILES[$request['input']]['name'] = $request['file']['name'][$at];
            $_FILES[$request['input']]['type'] = $request['file']['type'][$at];
            $_FILES[$request['input']]['tmp_name'] = $request['file']['tmp_name'][$at];
            $_FILES[$request['input']]['error'] = $request['file']['error'][$at];
            $_FILES[$request['input']]['size'] = $request['file']['size'][$at];

            // begin
            if (!$this->upload->do_upload($request['input'])) {
                $fails++;
            }
        }

        return $fails;
    }

}
