<?php

$lang['upload_userfile_not_set'] = "Tidak ditemukan berkas yang akan diunggah !!!";
$lang['upload_file_exceeds_limit'] = "Besar ukuran berkas melebihi batas ketentuan unggah pada pengaturan PHP !!!";
$lang['upload_file_exceeds_form_limit'] = "Besar ukuran berkas melebihi batas ketentuan unggah pada pengaturan Aplikasi !!!";
$lang['upload_file_partial'] = "Berkas unggahan hanya sebagian saja !!!";
$lang['upload_no_temp_directory'] = "Temporary folder tidak ditemukan !!!";
$lang['upload_unable_to_write_file'] = "Berkas tidak dapat disimpan dalam server !!!";
$lang['upload_stopped_by_extension'] = "Proses unggah terhenti karena tipe berkas tidak valid !!!";
$lang['upload_no_file_selected'] = "Tidak ada berkas !!!";
$lang['upload_invalid_filetype'] = "Tipe berkas tidak diperbolehkan untuk di-unggah !!!";
$lang['upload_invalid_filesize'] = "Berkas melebihi ketentuan batas maksimum besar ukuran !!!";
$lang['upload_invalid_dimensions'] = "Gambar melebihi ketentuan batas maksimum dimensinya !!!";
$lang['upload_destination_error'] = "Terjadi masalah ketika mencoba untuk memindahkan berkas unggahan ke tujuan akhir !!!";
$lang['upload_no_filepath'] = "Lokasi unggahan tidak valid !!!";
$lang['upload_no_file_types'] = "Anda belum menentukan jenis berkas yang diijinkan !!!";
$lang['upload_bad_filename'] = "Nama berkas yang Anda kirimkan sudah ada di server !!!";
$lang['upload_not_writable'] = "Direktori penyimpanan tidak dapat menyimpan berkas unggahan !!!";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */