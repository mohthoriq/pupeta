<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Copyright (C) 2015 Muhammad Iqbal (市丸 零) <jms21maru@gmail.com>
 * @author Muhammad Iqbal (市丸 零) <jms21maru@gmail.com>
 */

require_once APPPATH . '/third_party/PHPExcel.php';

class ExcelUtil extends PHPExcel {
    public function __construct() {
        parent::__construct();
    }
}