<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="ie9">
    <head>
        <?php include_once(APPPATH . '/views/_html_peta/_head.php'); ?></head>
    <style>

        path{
            fill: white;
        }

        .ln{
            fill:grey;
        }

        .circle {
            margin: 5px 0;
            border: 1px solid;
            border-radius:5%;
            font-size: 25px;
            width:40px;
            height:40px;
            padding:0 0 0 8px;
            cursor:pointer;
            background-color:white;
        }

        .subrow{
            font-size:12px;
            text-align:center;
        }

        .tabel-desc{
            font-size: 12px;
        }

        .tabel-desc th{
            padding:3px 0;
        }

        .tabel-desc th:nth-child(1){
            width:200px;
        }

        .tabel-desc th:nth-child(2){
            width:20px;
        }

        .tabel-desc tr:nth-child(1){
            font-size: 17px;
            font-weight: bold;
        }
        /* Center */
        .subrow-body td:nth-child(1), td:nth-child(3), td:nth-child(4){text-align:center;}
        /* Right */
        .subrow-body td:nth-child(5), td:nth-child(6), td:nth-child(7), td:nth-child(10){text-align:right;}
        /* Left */
        .subrow-body td:nth-child(2){text-align:left;}
    </style>
    <body class="body">
        <header id="header" class="clearfix" style="box-shadow: 0 1px 4px rgba(0,0,0,.3);">
            <nav class="navbar navbar-expand-md navbar-dark" style="background-color: #2196F3; min-height:70px">
                <div class="circle"><div class="arrow fas fa-arrow-left"></div></div>
            </nav>
        </header>

        <section id="main" class="mt-5 mx-5" style="margin:0 120px;">
            <h4>TABEL MONITORING PROGRES KEUANGAN DAN FISIK</h4>
            <?php
            $kodeProvinsi = $this->uri->segment(5);
            if ($kodeProvinsi !== FALSE) {
            $dataProgres = $this->model->getRecord(array('table' => 'data_progres_info', 'where' => array('provinsi' => strtoupper($kodeProvinsi))));
            $dataProvinsi = $this->model->getRecord(array('table' => 'data_provinsi', 'where' => array('kode_provinsi' => strtoupper($kodeProvinsi))));
            $dataBodo = $this->model->getRecord(array('table' => 'data_dummies', 'where' => array('provinsi' => strtoupper($kodeProvinsi))));
            $paguInt = 0;
            $kontrakInt = 0;
            $realisasiInt = 0;
            $realisasiPersen = 0.0;

            if ($dataBodo != NULL) {
                $paguInt = $dataBodo->pagu;
                $realisasiInt = $dataBodo->realisasi_uang_rp;
                $realisasiPersen = $dataBodo->realisasi_fisik_persen;
            }

            if ($dataProgres != NULL) {
                foreach ($this->model->getList(array('table' => 'data_progres_isi', 'where' => array('progres' => $dataProgres->kode))) as $dataIsi) {
                    $kontrakInt += $dataIsi->nilai;
                    $realisasiInt = $dataIsi->realisasi_uang_rp;
                }

                $paguInt = $dataProgres->pagu;
                $realisasiPersen = $dataProgres->total_realisasi_fisik;
            }
            ?>
            <table class="table table-borderless tabel-desc">
                <tbody>
                    <tr><th>Provinsi</th><th>:</th><th><?php echo strtoupper($dataProvinsi->nama); ?></th></tr>
                    <tr><th>Nilai Pagu</th><th>:</th><th id="nil-pagu">Rp <?php echo number_format($paguInt,0,",",".") ?></th></tr>
                    <tr><th>Nilai Kontrak</th><th>:</th><th id="nil-kontrak">Rp <?php echo number_format($kontrakInt ,0,",",".") ?></th></tr>
                    <tr><th>Nilai Realisasi Keuangan</th><th>:</th><th>Rp <?php echo number_format($realisasiInt ,0,",",".") ?></th></tr>
                    <tr><th>Nilai Realisasi Fisik</th><th>:</th><th><?php echo percent_format($realisasiPersen)?></th></tr>
                <tbody>
            </table>

            <table id="dt_basic" class="table table-light table-bordered table-striped" width="100%">
                <thead class="thead-dark">
                    <tr class="mainrow">
                        <th rowspan="2" scope="col">No</th>
                        <th rowspan="2" scope="col">Nama Paket</th>
                        <th rowspan="2" scope="col">K/S/AU</th>

                        <th rowspan="2" scope="col">SYC / MYC</th>
                        <th rowspan="2">Nilai Pagu</th>
                        <th rowspan="2">Nilai Kontrak</th>

                        <th colspan="3" style="font-size:13px; text-align:center;">Rencana</th>
                        <th colspan="3" style="font-size:13px; text-align:center;">Realisasi</th>
                        <th rowspan="2"></th>
                    </tr>
                    <tr class="subrow">
                        <th>Keu (Rp)</th>
                        <th>Keu (%)</th>
                        <th>Fis (%)</th>
                        <th>Keu (Rp)</th>
                        <th>Keu (%)</th>
                        <th>Fis (%)</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if ($dataProgres != NULL) {
                    $i = 1;
                    foreach ($this->model->getList(array('table' => 'data_progres_isi', 'where' => array('progres' => $dataProgres->kode))) as $record) {
                        ?>
                        <tr class="subrow subrow-body">
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $record->paket; ?></td>
                            <td><?php echo $record->ksau; ?></td>
                            <td><?php echo $record->sycmyc; ?></td>
                            <td><?php echo number_format($record->pagu,0,",","."); ?></td>
                            <td><?php echo number_format($record->nilai,0,",","."); ?></td>

                            <td><?php echo number_format($record->rencana_uang_rp,0,",","."); ?></td>
                            <td><?php echo percent_format($record->rencana_uang_persen); ?></td>
                            <td><?php echo percent_format($record->rencana_fisik_persen); ?></td>

                            <td><?php echo number_format($record->realisasi_uang_rp,0,",","."); ?></td>
                            <td><?php echo percent_format($record->realisasi_uang_persen); ?></td>
                            <td><?php echo percent_format($record->realisasi_fisik_persen); ?></td>
                            <td><a href="<?php echo site_url('modul/tampil/info_rincian/peta').'/'.$record->kode; ?>" class="btn btn-info">Detail</a></td>
                        </tr>
                        <?php
                    }
                }
                
                ?>
                </tbody>
            </table>
            <?php } ?>

        </section>
        <?php include_once(APPPATH . '/views/_html_peta/_script.php'); ?>
    </body>
</html>
<?php function percent_format($value){ return $value <= 0?"": number_format($value,2,",",".").' %';} ?>
<script>

    $('.circle').click(function (e) {
        // $(location).attr('href', "<?php echo site_url(); ?>");
        window.history.back();
    });

</script>