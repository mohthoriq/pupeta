<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="ie9">
    <head>
    <?php include_once(APPPATH . '/views/_html_peta/_head.php'); ?></head>
    <style>

        path{
            fill: white;
        }

        .ln{
            fill:grey;
        }

        .circle {
            margin: 5px 0;
            border: 1px solid;
            border-radius:5%;
            font-size: 25px;
            width:40px;
            height:40px;
            padding:0 0 0 8px;
            cursor:pointer;
            background-color:white;
        }

        .subrow{
            font-size:12px;
        }

        .tabel-desc{
            font-size: 12px;
        }

        .tabel-desc th{
            padding:3px 0;
        }

        .tabel-desc th:nth-child(1){
            width:200px;
        }

        .tabel-desc th:nth-child(2){
            width:20px;
        }

        .tabel-desc tr:nth-child(1){
            font-size: 17px;
            font-weight: bold;
        }
        
        /* Center */
        .subrow-body td:nth-child(1), td:nth-child(9), td:nth-child(10), td:nth-child(12){text-align:center;}
        /* Right */
        .subrow-body td:nth-child(5), td:nth-child(6), td:nth-child(7), td:nth-child(8), td:nth-child(11){text-align:right;}

    </style>
    <body class="body">
    <header id="header" class="clearfix" style="box-shadow: 0 1px 4px rgba(0,0,0,.3);">
        <nav class="navbar navbar-expand-md navbar-dark" style="background-color: #2196F3">
            <div class="circle"><div class="arrow fas fa-arrow-left"></div></div>
        </nav>
    </header>
    
    <section id="main" class="mt-5 mx-5 ">
        <h4>RINCIAN PROGES KEUANGAN DAN FISIK PER UNIT SEKOLAH</h4>
        <table class="table table-borderless tabel-desc">
            <tbody>
                <?php
                $rincian = $this->uri->segment(5);
                $dataMonitoring;
                if ($rincian !== FALSE) {
                    $dataMonitoring = $this->model->getRecord(array(
                        'table' => 'data_progres_isi',
                        'where' => array('kode' => $rincian)
                    ));

                    $dataInfo = $this->model->getRecord(array(
                        'table' => 'data_progres_info',
                        'where' => array('kode' => $dataMonitoring->progres)
                    ));

                    $dataProvinsi = $this->model->getRecord(array(
                        'table' => 'data_provinsi',
                        'where' => array('kode_provinsi' => $dataInfo->provinsi)
                    ));
                    


                ?>
                <tr><th>Provinsi</th><th>:</th><th><?php echo $dataProvinsi->nama?></th></tr>
                <tr><th>Paket</th><th>:</th><th><?php echo $dataMonitoring->paket?></th></tr>
                <tr><th>Mekanisme Kontrak</th><th>:</th><th><?php echo $dataMonitoring->sycmyc?></th></tr>
                <tr><th>Jumlah Sarana</th><th>:</th><th id="jumlah"></th></tr>
                <?php }?>
            <tbody>
        </table>
        <table id="dt_basic" class="table table-light table-bordered table-striped" width="100%">
            <thead class="thead-dark">
                <tr class="mainrow">
                    <th rowspan="2" scope="col">No</th>
                    <th rowspan="2" scope="col">Sarana</th>
                    <th rowspan="2" scope="col">Lokasi</th>
                    <th rowspan="2" scope="col">Jenjang</th>
                    <th rowspan="2">Nilai Pagu</th>
                    <th colspan="2" style="font-size:13px; text-align:center;">Kontrak</th>
                    <th colspan="3" style="font-size:13px; text-align:center;">Rencana</th>
                    <th colspan="3" style="font-size:13px; text-align:center;">Realisasi</th>
                </tr>
                <tr class="subrow">
                    <th>Nilai</th>
                    <th>Adenium</th>
                    <th>Keu (Rp)</th>
                    <th>Keu (%)</th>
                    <th>Fis (%)</th>
                    <th>Keu (Rp)</th>
                    <th>Keu (%)</th>
                    <th>Fis (%)</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            if ($rincian !== FALSE) {
                foreach ($this->model->getList(array(
                    'table' => 'data_progres_rinci', 
                    'where' => array('isi' => $rincian)
                )) as $record) {
                ?>
                <tr class="subrow subrow-body">
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $record->sarana; ?></td>
                    <td><?php echo $record->lokasi; ?></td>
                    <td><?php echo $record->jenjang; ?></td>
                    
                    <td><?php echo number_format($record->pagu,0,",","."); ?></td>
                    <td><?php echo number_format($record->nilai,0,",","."); ?></td>
                    <td><?php echo number_format($record->revisi,0,",","."); ?></td>

                    <td><?php echo number_format($record->rencana_uang_rp,0,",","."); ?></td>
                    <td><?php echo percent_format($record->rencana_uang_persen); ?></td>
                    <td><?php echo percent_format($record->rencana_fisik_persen); ?></td>
                    <td><?php echo number_format($record->realisasi_uang_rp,0,",","."); ?></td>
                    <td><?php echo percent_format($record->realisasi_uang_persen); ?></td>
                    <td><?php echo percent_format($record->realisasi_fisik_persen); ?></td>
                </tr>
            <?php 
                }
            }
            ?>
            </tbody>
        </table>
    </section>
    <input id="jumlah_temp" type="hidden" value="<?php echo $i; ?>">
    <?php include_once(APPPATH . '/views/_html_peta/_script.php'); ?>
    </body>
</html>
<?php function percent_format($value){ return $value <= 0?"": number_format($value,2,",",".").' %';} ?>
<script>

    $('#jumlah').text($('#jumlah_temp').val()+' Unit');

    $('.circle').click(function(e){
        // $(location).attr('href', "<?php echo site_url(); ?>");
        window.history.back();
    })

</script>