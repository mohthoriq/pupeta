<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Data Peta - Progres </title>

<!-- Vendor CSS -->
<!-- <link href="<?php echo base_url('res/vendors/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet"> -->
<link href="<?php echo base_url('res/vendors/bower_components/animate.css/animate.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/nouislider/distribute/nouislider.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/dropzone/dist/min/dropzone.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/farbtastic/farbtastic.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/bower_components/chosen/chosen.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/vendors/summernote/dist/summernote.css'); ?>" rel="stylesheet">

<!-- JQVMap -->
<link href="<?php echo base_url('res/vendors/jqvmap/jqvmap.min.css'); ?>" rel="stylesheet">

<!-- Tippy (Tooltip)-->
<link href="<?php echo base_url('res/vendors/tippyjs/tippy.css'); ?>" rel="stylesheet">

<!-- CSS -->
<link href="<?php echo base_url('res/css/app_1.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('res/css/app_2.min.css'); ?>" rel="stylesheet">

<!-- File Input -->
<link href="<?php echo base_url('res/vendors/bootstrap-fileinput/css/fileinput.min.css'); ?>" rel="stylesheet">

<!-- Font Awsome -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/fontawesome-5.11.2/css/fontawesome.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/fontawesome-5.11.2/css/regular.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/fontawesome-5.11.2/css/solid.min.css'); ?>">