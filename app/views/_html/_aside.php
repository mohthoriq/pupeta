<aside id="sidebar" class="sidebar c-overflow">
    <div class="s-profile">
        <a href="" data-ma-action="profile-menu-toggle">
            <div class="sp-pic">
                <img src="<?php echo base_url('res/img/pp.jpg'); ?>" alt="">
            </div>

            <div class="sp-info">
                System Development
            </div>
        </a>
    </div>

    <ul class="main-menu">
        <li><a href="<?php echo site_url(); ?>"><i class="zmdi zmdi-map"></i> Peta </a></li>
        <li><a href="<?php echo site_url('modul/tampil/peta/data'); ?>"><i class="zmdi zmdi-file-add"></i> Unggah Data </a></li>
    </ul>
</aside>