<header id="header" class="clearfix" data-ma-theme="blue">
    <ul class="h-inner">
        <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="hi-logo hidden-xs">
            <a href="<?php echo site_url('modul/tampil/peta/data'); ?>"> Kelola Data Peta </a>
        </li>

        <li class="pull-right">
            <ul class="hi-menu">
            </ul>
        </li>
    </ul>
</header>