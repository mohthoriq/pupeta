<!-- Javascript Libraries -->
<script src="<?php echo base_url('res/vendors/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>

<script src="<?php echo base_url('res/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/Waves/dist/waves.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bootstrap-growl/bootstrap-growl.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js'); ?>"></script>

<script src="<?php echo base_url('res/vendors/bower_components/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/nouislider/distribute/nouislider.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/dropzone/dist/min/dropzone.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/summernote/dist/summernote-updated.min.js'); ?>"></script>

<script src="<?php echo base_url('res/vendors/bower_components/flot/jquery.flot.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/flot/jquery.flot.resize.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/flot/jquery.flot.pie.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/flot-orderbars/jquery.flot.orderBars.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/flot.curvedlines/curvedLines.js'); ?>"></script>

<!-- JQVMap -->
<script src="<?php echo base_url('res/vendors/jqvmap/jquery.vmap.js'); ?>" type="text/javascript" ></script>
<script src="<?php echo base_url('res/vendors/jqvmap/maps/jquery.vmap.indonesia.js'); ?>" type="text/javascript"  charset="utf-8"></script>

<script src="<?php echo base_url('res/vendors/popper-1.12/popper.js'); ?>" type="text/javascript"  charset="utf-8"></script>
<script src="<?php echo base_url('res/vendors/tippyjs/tippy.js'); ?>" type="text/javascript"  charset="utf-8"></script>

<!-- File Input -->
<script src="<?php echo base_url('res/vendors/bootstrap-fileinput/js/fileinput.min.js'); ?>"></script>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
    <script src="<?php echo base_url('res/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js'); ?>"></script>
<![endif]-->

<script src="<?php echo base_url('res/vendors/bower_components/chosen/chosen.jquery.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/farbtastic/farbtastic.min.js'); ?>"></script>

<script src="<?php echo base_url('res/js/app.min.js'); ?>"></script>
<script>
    function dungDung(text, url) {
        $.growl({
            icon: 'fa fa-check',
            title: ' -',
            message: text,
            url: url
        }, {
            element: 'body',
            type: 'success',
            allow_dismiss: true,
            placement: {from: 'top', align: 'center'},
            offset: {x: 20, y: 85},
            spacing: 10,
            z_index: 1031,
            delay: 2500,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            animate: {enter: 'animated fadeInUp', exit: 'animated fadeOutUp'},
            icon_type: 'class',
            template: '<div data-growl="container" class="alert" role="alert">' +
                    '<button type="button" class="close" data-growl="dismiss">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '<span class="sr-only">Close</span>' +
                    '</button>' +
                    '<span data-growl="icon"></span>' +
                    '<span data-growl="title"></span>' +
                    '<span data-growl="message"></span>' +
                    '<a href="#" data-growl="url"></a>' +
                    '</div>'
        });
    }
</script>