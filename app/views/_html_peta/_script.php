<!-- Javascript Libraries -->
<script src="<?php echo base_url('res/vendors/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('res/vendors/popper-1.12/popper.js'); ?>" type="text/javascript"  charset="utf-8"></script>
<script src="<?php echo base_url('res/vendors/bootstrap-4.3/dist/js/bootstrap.min.js'); ?>"></script>

<!-- Tippy ( Tooltip ) -->
<script src="<?php echo base_url('res/vendors/tippyjs/tippy.js'); ?>" type="text/javascript"  charset="utf-8"></script>

<!-- Data Tables -->
<script src="<?php echo base_url('res/vendors/datatables-1.10/datatables.min.js'); ?>" type="text/javascript"  charset="utf-8"></script>

<script>
var globalDTOptions = {
    "deferRender": true,
    "ordering": false,
    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
    "autoWidth": true,
    "oLanguage": {
        "sSearch": '<span class="input-group-addon"><i class="fa fa-search"></i></span>',
        'sLengthMenu': 'Menampilkan _MENU_ data', 'sInfo': 'Menampilkan data _START_ - _END_ / ( _TOTAL_ )',
        'sInfoFiltered': '(hasil filter dari _MAX_ data)',
        'sZeroRecords': 'Tidak ada data', 'sInfoEmpty': 'Tidak ada data', 'sEmptyTable': 'Tidak ada data',
        'sLoadingRecords': 'Tunggu beberapa saat...', 'sProcessing': 'Memproses...',
        'oPaginate': {
            'sFirst': '<i class="fa fa-angle-double-left fa-xs"></i>',
            'sLast': '<i class="fa fa-angle-double-right fa-xs"></i>',
            'sNext': '<i class="fa fa-angle-right fa-xs"></i>', 'sPrevious': '<i class="fa fa-angle-left fa-xs"></i>'
        }
    }
};
</script>