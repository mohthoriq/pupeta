<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard Viewer</title>

<!-- Vendor CSS -->
<link href="<?php echo base_url('res/vendors//bootstrap-4.3/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">

<!-- Font Awsome -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/fontawesome-5.11.2/css/fontawesome.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/fontawesome-5.11.2/css/regular.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/fontawesome-5.11.2/css/solid.min.css'); ?>">

<!-- Data Tables -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('res/vendors/datatables-1.10/datatables.min.css'); ?>">