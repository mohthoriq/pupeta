<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="ie9">
    <head><?php include_once(APPPATH . '/views/_html/_head.php'); ?></head>
    <body>
        <?php include_once(APPPATH . '/views/_html/_header.php'); ?>

        <section id="main">
            <?php include_once(APPPATH . '/views/_html/_aside.php'); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>
                            Peta Indonesia <small>progres setiap provinsi.</small>
                        </h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>
                                Unggah Data Progres
                                <small>Unduh dahulu template excel yang sudah disesuaikan dengan proses pengolahan data di sistem ini.</small>
                            </h2>
                        </div>
                        <div class="card-body card-padding">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Unduh Template Excel</h3>
                                    <a href="<?php echo base_url('file/progres.xls'); ?>" class="btn bgm-lightgreen m-t-10">Progress</a>
                                    <a href="<?php echo base_url('file/rincian.xls'); ?>" class="btn bgm-green m-t-10">Detail</a>
                                    <div class="clearfix"></div>
                                    <h3>Unggah Berkas Excel</h3>
                                    <button type="button" class="btn bgm-lightblue m-t-10" id="progres-btn">Progress</button>
                                    <button type="button" class="btn bgm-blue m-t-10" id="detail-btn">Detail</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <!-- Modal Untuk Document Upload -->
                            <div class="modal fade" id="modal-document">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title">Unggah Berkas</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="*" />
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                    </div>
                </div>
            </section>
        </section>

        <?php include_once(APPPATH . '/views/_html/_footer.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var tipe = null;

            $(document).ready(function () {
                initUploader('#filedocument', 'etc', 'data');
                $('#progres-btn').click(function () {
                    $('#modal-document').modal();
                    tipe = 'progres';
                });
                $('#detail-btn').click(function () {
                    $('#modal-document').modal();
                    tipe = 'rincian';
                });
            });

            function initUploader(id, path_parent, path_dir) {
                $(id).fileinput({
                    maxFileCount: 1, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-default btn-flat", browseLabel: "Browse", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-warning btn-flat", removeLabel: "Delete", removeIcon: '<i class="fas fa-trash"></i> ',
                    uploadClass: "btn btn-primary btn-flat", uploadLabel: "Upload", uploadIcon: '<i class="fa fa-cloud-upload-alt"></i> ',
                    showCaption: false,
                    uploadExtraData: function () {
                        return {path: path_parent, dir: path_dir};
                    }
                });

                $(id).on('fileuploaded', function () {
                    var url = '<?php echo site_url('modul/proses'); ?>/' + tipe;
                    $(id).fileinput('clear');
                    $('#modal-document').modal('hide');
                    dungDung('Data ' + tipe.toUpperCase(), url);
                    setTimeout(function () {
                        $(location).attr('href', url);
                    }, 3000);
                });
            }
        </script>
    </body>
</html>