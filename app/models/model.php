<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @author Muhammad Iqbal (市丸 零) <jms21maru@gmail.com> - Copyright (C) 2016
 */
class Model extends CI_Model {

    public $CREATE = 1, $UPDATE = 2, $DELETE = 3; // action flag
    protected $table = ''; // for child
    private $numData = 0; // num of record
    private $exceptions = array('action-input', 'model-input', 'key-input');
    private $inputs = array();
    private $pointer = array();

    public function __construct() {
        parent::__construct();
    }

    /* ------------------------ SCENARIO - get ------------------------ */
    /*
      | ---------- $query is an array ----------
      | key index = table, select, where, sort, limit
     */

    public function getRecord($query = array()) { // get record
        $record = null;

        if (isset($query['table'])) {
            $select = (isset($query['select'])) ? $query['select'] : null;
            $where = (isset($query['where'])) ? $query['where'] : null;
            $sort = (isset($query['sort'])) ? $query['sort'] : null;
            self::_initClause(array(
                'select' => $select, 'where' => $where, 'sort' => $sort
            ));
            $data = $this->db->get($query['table']);
            $this->numData = $data->num_rows();
            $record = ($this->numData == 0) ? null : $data->row();
        }

        return $record;
    }

    public function getList($query = array()) { // get list
        $list = array();

        if (isset($query['table'])) {
            $select = (isset($query['select'])) ? $query['select'] : null;
            $where = (isset($query['where'])) ? $query['where'] : null;
            $sort = (isset($query['sort'])) ? $query['sort'] : null;
            self::_initClause(array(
                'select' => $select, 'where' => $where, 'sort' => $sort
            ));
            self::_initList($query);
            $data = $this->db->get($query['table']);
            $this->numData = $data->num_rows();
            $list = ($this->numData == 0) ? array() : $data->result();
        }

        return $list;
    }

    private function _initClause($clause = array()) { // $clause is an array
        if (isset($clause['select'])) { // set SELECT portion
            if ($clause['select'] != null) {
                $this->db->select($clause['select']); // string type
            }
        }

        if (isset($clause['where'])) { // set WHERE clause
            if ($clause['where'] != null) {
                $this->db->where($clause['where']); // array or string type
            }
        }

        if (isset($clause['sort'])) { // set an ORDER BY clause
            if ($clause['sort'] != null) {
                $this->db->order_by($clause['sort']); // string type
            }
        }
    }

    private function _initList($clause = array()) {
        if (isset($clause['limit'])) { // $this->db->limit(limit, offset);
            $limit = $clause['limit']; // > as array
            $this->db->limit($limit[0]);

            if (isset($limit[1])) {
                $this->db->limit($limit[0], $limit[1]);
            }
        }

        if (isset($clause['group'])) {
            $this->db->group_by($clause['group']); // string or array
        }

        if (isset($clause['find'])) {
            if ($clause['find'] != NULL) {
                $this->db->like($clause['find']); // string or array
            }
        }
    }

    /* ------------------------ SCENARIO - child class implementation ------------------------ */

    public function getTable() {
        return $this->table;
    }

    public function getNumData() { // get num of record
        return $this->numData;
    }

    public function getLastID() { // get 'id' (PK) of last executed query
        return $this->db->insert_id();
    }

    /* ------------------------ SCENARIO - set/manage ------------------------ */
    /*
      | ------- $query is an array -------
      | key index = table, type, data, at
     */

    public function action($query = array()) { // action record
        $result = false;

        if (isset($query['table']) && isset($query['type'])) {
            switch ($query['type']) {
                case $this->CREATE:
                    $result = self::_doCreate($query);
                    break;
                case $this->UPDATE:
                    $result = self::_doUpdate($query);
                    break;
                case $this->DELETE: // delete ;)
                    $result = self::_doDelete($query);
                    break;
            }
        }

        return $result;
    }

    private function _doCreate($query) {
        $result = false;

        if (isset($query['data'])) {
            $result = $this->db->insert($query['table'], $query['data']);
        }

        return $result;
    }

    private function _doUpdate($query) {
        $result = false;

        if (isset($query['at']) && isset($query['data'])) {
            $this->db->where($query['at']); // at is an array
            $result = $this->db->update($query['table'], $query['data']);
        }

        return $result;
    }

    private function _doDelete($query) {
        $result = false;

        if (isset($query['at'])) {
            $result = $this->db->delete($query['table'], $query['at']); // at is an array
        }

        return $result;
    }

    /* ------------------------ SCENARIO - action implementation ------------------------ */

    public function setValues($params, $uniques = array()) {
        foreach ($params as $key => $param) {
            if (in_array($key, $this->exceptions)) {
                $this->pointer[str_replace('-input', '', $key)] = $param;
            } else {
                self::_setInputs($key, $uniques, $params['action-input'], $param);
            }
        }
    }

    public function setValue($key, $value) {
        $this->inputs[$key] = $value;
    }

    public function clearValues() {
        $this->pointer = array();
        $this->inputs = array();
    }

    public function doSave() {
        $query = array(
            'table' => $this->table, 'type' => $this->pointer['action'],
            'data' => $this->inputs, 'at' => array('entitas' => $this->pointer['key'], 'kode' => $this->inputs['kode']) // clause for model
        );

        return $this->action($query);
    }

    private function _setInputs($key, $uniques, $action, $param) {
        $avoid = true;

        if (in_array($key, $uniques)) { // avoid unique values
            if ($action == $this->CREATE) {
                $avoid = false;
            }
        } else {
            $avoid = false;
        }

        if (!$avoid) {
            $this->inputs[str_replace('-input', '', $key)] = $param;
        }
    }

}
